# array_a = [1, 2, 3, 4, 5, 6]
# array_b = [7, 8, 9, 1, 2, 3]
# array_a = [1, 2, 3]
# array_b = [4, 5, 6, 1, 2, 3]
# array_a = [1, 2, 3, 4]
# array_b = [4, 1, 2, 1, 2, 3]
# array_a = [1, 2, 3, 4]
# array_b = [2, 1, 2, 1, 2, 3, 4]
# array_a = [1, 2, 3, 4, 6]
# array_b = [2, 1, 2, 1, 2, 3, 4]
array_a = [4, 2, 5, 4, 6, 8, 9]  # J
array_b = [2, 1, 9, 5, 4, 6, 4]  # Q


# array_a = [1, 2]
# array_b = [0, 1, 2]


def find_longest_common_subarray(array_a, array_b):
    if len(array_a) >= len(array_b):
        smaller_array = array_b
        bigger_array = array_a
    else:
        smaller_array = array_a
        bigger_array = array_b

    x = 0
    count = 0
    q = 0
    j = 0
    index_ini_a = 0
    index_ini_b = 0
    subarray_size = 1

    while x < len(smaller_array):
        # print("\nX:", x)
        while q < len(smaller_array) and j < len(bigger_array):  # j?
            # if q == 3 and j == 2:
            #     print("debugg")
            # print("\nQ:", q)
            # print("J:", j)
            if smaller_array[q] == bigger_array[j]:
                # print("igualdad:", smaller_array[q], "==", bigger_array[j])
                count += 1
                if count == 1:
                    index_ini_a = q
                    index_ini_b = j
                if (q == (len(smaller_array) - 1) or j == (len(bigger_array) - 1)) and count > subarray_size:
                    # print("adentro")
                    subarray_size = count
                    result_a = smaller_array[index_ini_a:q + 1]
                    result_b = bigger_array[index_ini_b:j + 1]
                    return result_a, result_b
                q += 1
                j += 1
            else:
                # print("(else) count:", count)
                if count > subarray_size:
                    # print("count:", count, ">", subarray_size, "subarray_size")
                    subarray_size = count
                    subarray_ini_a = index_ini_a
                    subarray_ini_b = index_ini_b
                    subarray_end_a = q
                    subarray_end_b = j

                    # print("subarray a:", smaller_array[subarray_ini_a:subarray_end_a])
                    # print("subarray b:", bigger_array[subarray_ini_b:subarray_end_b])
                if count == 0:
                    j += 1
                else:
                    count = 0
                    j = index_ini_b + 1
                q = x
        x += 1
        q = x
        j = 0
        count = 0

    result_a = smaller_array[subarray_ini_a:subarray_end_a]
    result_b = bigger_array[subarray_ini_b:subarray_end_b]

    return result_b, result_a


result_a, result_b = find_longest_common_subarray(array_a, array_b)
print("result:", result_a)
print("result:", result_b)
