array = [1, 2, 3, 8, 2, 5]
# array = [1, 2, 3, 8, 2, 4]
# array = [1, 1, 1, 1, 1, 4]
number = 7


def find_subarray_sum(given_number, array):
    k = 0
    j = 0
    subarray_indexes = []
    sum = 0

    while sum != given_number and k <= len(array) - 1:
        if sum < given_number and j <= len(array) - 1:
            sum = sum + array[j]
            j += 1
            subarray_indexes.append(j-1)
        else:
            sum = 0
            subarray_indexes = []
            k += 1
            j = k

    if sum == given_number:
        return subarray_indexes  # mejor poner a que imprima el subarray de una vez.(aqui imprime indices) 🤗🤣
    else:
        return "there was no subarray that match the sum"


resutl = find_subarray_sum(number, array)
print(resutl)
