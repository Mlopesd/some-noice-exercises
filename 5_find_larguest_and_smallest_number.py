array = [17, 18, 1, 10, 14, 16, 2, 3, 11, 4, 5, 12, 6, 13, 7, 8, 14, 9, 19, 20]
# array = [1, 2, 3, 3, 3, 1, 4, 4, 5, 3, 2, 5, 2]


def find_largest_and_smallest_number(array):
    largest_number = array[0]
    smallest_number = array[0]
    x = 0

    for x in range(0, len(array) - 1):
        if largest_number < array[x + 1]:
            largest_number = array[x + 1]
        if smallest_number > array[x + 1]:
            smallest_number = array[x + 1]

    return largest_number, smallest_number


max_number, min_number = find_largest_and_smallest_number(array)
print("\nlargest number", max_number, "\nsmallest number:", min_number)
