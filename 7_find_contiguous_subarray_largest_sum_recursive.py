array = [1, 2, 3, 8, 2, 5, 2]
# array = [2, 2, 2, 1, 1, 1, 1, 3, 3, 3, 1, 1, 1, 1, 4, 4, 4, 1, 1, 1, 1, 9, 9, 9]
# array = [1, 2, 3, 8, 2, 4]
size = 7


def find_largest_subarray_sum(size, array, k, x, suma, total, subarray_index, subarray):
    if size > len(array):
        return print("ERROR: 'size' it's bigger than array's length.")
    elif size == len(array):
        total = sum(array)
        return print("\ntotal sum:", total, ", sub array:", array, "(the entire array 😆)")

    if x == size:
        for j in range(0, size):
            suma = suma + array[j]

    # the actual recursion
    if x == len(array):
        subarray = array[subarray_index: subarray_index + size]
        print("final de rec:", subarray, "total:", total)
        return total, subarray
    elif x < len(array):
        if total < suma:
            total = suma
            subarray_index = k

        suma = (suma - array[k]) + array[x]
        find_largest_subarray_sum(size, array, k + 1, x + 1, suma, total, subarray_index, subarray)


k = 0
suma = 0
total = 0
subarray_index = 0
subarray = []

find_largest_subarray_sum(size, array, k, size, suma, total, subarray_index, subarray)

