from: ---> https://twitter.com/svpino/status/1354048200601198593

https://buzzchronicles.com/SteveeRogerr/b/tech/10255/

1. Write a function that reverses an array in place.
In other words, the function should not use an auxiliary array to do the work.

2. Write a function that finds the missing number in an unsorted array containing every one of the
 other 99 numbers ranging from 1 to 100.

3. Write a function that finds the duplicate number in an unsorted array containing every
 number from 1 to 100.
Only one of the numbers will appear twice.

4. Write a function that removes every duplicate value in an array.
There could be more than one value duplicated. You should remove all of them leaving a
 single copy of the value.

5. Write a function that finds the largest and smallest number in an unsorted array.

6. Write a function that finds a subarray whose sum is equal to a given value.

7. Write a function that finds the contiguous subarray of a given size with the largest sum.

8. Write a function that, given two arrays, finds the longest common
subarray present in both of them.

9. Write a function that, given two arrays, finds the length of the shortest array that
contains both input arrays as subarrays.

10. Write a function that, given an array, determines if you can partition it
in two separate subarrays such that the sum of elements in both subarrays is the same.

11. Write a function that, given an array, divides it into two subarrays,
 such as the absolute difference between their sums is minimum.