# array = [2, 7, 3, 5, 1]
array = [3, 4, 8, 1, 2, 3, 4, 5]


# even length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # ini
# array = [5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # ini otro
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # fin
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5]  # fin otro
# array = [1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 1]  # mitad izq
# array = [1, 1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
# array = [1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1]  # igual


# odd length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 2]  # ini
# array = [6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # ini otro
# array = [2, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # fin
#          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2]  # fin otro largo len=11
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6]  # fin otro
# array = [1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 2]  # mitad izq
# array = [2, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
# array = [1, 1, 1, 1, 5, 4, 1, 1, 1, 1, 1]  # igual


def can_be_partitioned(array, suma, x, len_array):
    half = int(len(array) / 2)
    if sum(array[:half]) > sum(array[half:]):
        if suma == sum(array[x:]):
            print("\nCan be partitioned, from 0 to {end}, and {end} to {len_array}".format(end=x, len_array=len(array)))
            print("subarray a: ", array[:x])
            print("subarray b: ", array[x:])
            print("Common sum:", suma)
            return
        elif suma > sum(array[x + 1:]):
            # if suma is greater than the rest oh the array,
            # there's no chance to find the subarray of equal sum anymore.
            print("it cannot be partitioned 🦏")
            return
        suma = suma + array[x]
        can_be_partitioned(array, suma, x + 1, len_array)
    elif sum(array[:half]) < sum(array[half:]):
        x = len_array-1
        suma = suma + array[x]
        if suma == sum(array[:x]):
            print("\nCan be partitioned, from 0 to {end} and {end} to {len_array}".format(end=x,
                                                                                        len_array=len(array)))
            print("subarray a: ", array[:x])
            print("subarray b: ", array[x:])
            print("Common sum:", suma)
            return
        elif suma > sum(array[:x - 1]):
            print("it cannot be partitioned 🦏")
            return
        # len_array -= 1
        can_be_partitioned(array, suma, x, len_array-1)
    else:
        print("\nCan be partitioned, at the half of the array. 🐧 (half_length={half})".format(half=half))
        print("subarray a: ", array[:half])
        print("subarray b: ", array[half:])
        print("Common sum:", sum(array[:half]))
        return


suma = 0
x = 0
len_array = len(array)
can_be_partitioned(array, suma, x, len_array)
