array = [1, 2, 3, 8, 2, 5, 2]
# array = [2, 2, 2, 1, 1, 1, 1, 3, 3, 3, 1, 1, 1, 1, 4, 4, 4, 1, 1, 1, 1, 9, 9, 9]
# array = [1, 2, 3, 8, 2, 4]
size = 3


def find_largest_subarray_sum(size, array):
    if size > len(array):
        total = 0
        subarray = "ERROR: 'size' it's bigger than array's length "
        return total, subarray
    elif size == len(array):
        total = sum(array)
        return total, array

    suma = 0
    for k in range(0, size):
        suma = suma + array[k]
    print("suma:", suma)
    total = suma
    k = 0
    subarray_index = k

    for x in range(size, len(array)):
        suma = (suma - array[k]) + array[x]

        if total < suma:
            subarray_index = k+1
            total = suma
        k += 1

    subarray = array[subarray_index:subarray_index + size]
    return total, subarray


total, subarray = find_largest_subarray_sum(size, array)
print("\ntotal sum:", total, ", sub array:", subarray)
