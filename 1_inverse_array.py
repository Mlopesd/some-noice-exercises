
array = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def inverse_array(array):
    k = -1
    for x in range(0, int(len(array) / 2)):
        a = array[x]
        array[x] = array[k]
        array[k] = a
        k -= 1

    return array


result_array = inverse_array(array)
print(result_array)
