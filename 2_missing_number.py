array = [17, 18, 1, 10, 14, 16, 2, 3, 11, 4, 5, 12, 6, 13, 7, 8, 9, 19, 20]     # missing: 15
# array_a = [4, 2, 1]
# array_a = [3, 4, 6, 2, 1]


def missing_value(array):
    array_b = list(array)
    max_value = len(array) + 1
    value = 1

    while value != max_value:
        for k in range(0, len(array_b)):
            if array_b[k] == value:
                if len(array_b) > 1:
                    array_b.pop(k)
                value += 1
                break
            elif k+1 == len(array_b) and value - 1 != max_value:
                return value

    return "actually, there is no missing number :)"


result_array = missing_value(array)
print("missing number:", result_array)
