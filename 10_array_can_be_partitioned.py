# array = [2, 7, 3, 5, 1]
# array = [3, 4, 8, 1, 2, 3, 4, 5]


# even length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # ini
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # fin
# array = [1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 1]  # mitad izq
# array = [1, 1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
# array = [1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1]  # igual


# odd length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 2]  # first half
# array = [2, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # second half
array = [1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 2]  # mitad izq


# array = [2, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
# array = [1, 1, 1, 1, 5, 4, 1, 1, 1, 1, 1]  # igual

# with negatives
# array = [27, 35, 12, 5, 42, 10, -60, 17, 22, 32]  # first half
# array = [14, -80, 20, 20, 20, 20, 2, 2, 2, 2, 2, 2, 2] # igual
# array = [10, -80, 20, 20, 20, 20, 2, 2, 2, 2, 2]


def can_be_partitioned(array):
    half = int(len(array) / 2)
    if sum(array[:half]) > sum(array[half:]):
        suma = array[0]
        for x in range(len(array) - 1):
            if suma == sum(array[x + 1:]):
                print("subarray a: ", array[:x + 1])
                print("subarray b: ", array[x + 1:])
                return \
                    "can be partitioned, from 0 to {end} and {end} to {len_array}". \
                        format(
                        end=x + 1,
                        len_array=len(array)
                    )
            # elif suma > sum(array[x + 1:]):
            # only works when array has only positive numbers.it was a nice try though
            # it save a lot of iterations btw.
            # if suma is greater than the rest oh the array,
            # there's no chance to find the subarray of equal sum anymore.
            #     return "it cannot be partitioned 🦏"
            suma = suma + array[x + 1]
        return "it cannot be partitioned 🦏"
    elif sum(array[:half]) < sum(array[half:]):
        suma = array[-1]
        for x in range(len(array), 0, -1):
            if suma == sum(array[:x - 1]):
                print("subarray a: ", array[:x - 1])
                print("subarray b: ", array[x - 1:])
                return \
                    "can be partitioned, from 0 to {end} and {end} to {len_array}". \
                        format(
                        end=x - 1,
                        len_array=len(array)
                    )
            # elif suma > sum(array[:x - 1]):
            # only works when array has only positive numbers.it was a nice try though
            #     return "it cannot be partitioned 🦏"
            suma = suma + array[x - 1]
        return "it cannot be partitioned 🦏"
    else:
        return \
            "can be partitioned, at the half of the array. 🐧 (half_length={half})" .format(half=half)


result = can_be_partitioned(array)
print(result)
