# array = [2, 7, 3, 5, 1]
# array = [3, 4, 8, 1, 2, 3, 4, 5]
# array = [1, 21, 1, 5]


# even length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # ini
# array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # fin
# array = [1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 1]  # mitad izq
# array = [1, 1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
#        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
# array = [1, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1, 1]  # igual


# odd length
# array = [5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 2]  # ini
# array = [2, 1, 1, 1, 1, 1, 1, 1, 1, 5, 5]  # fin
# array = [1, 1, 1, 5, 5, 1, 1, 1, 1, 1, 2]  # mitad izq
# array = [2, 1, 1, 1, 1, 5, 5, 1, 1, 1, 1]  # mitad der
# array = [1, 1, 1, 1, 5, 4, 1, 1, 1, 1, 1]  # igual

# with negatives
# array = [27, 35, 12, 5, 42, 10, -60, 17, 22, 32]  # first half
# array = [27, -35, 12, 5, 42, 10, 60, 17, 22, 32]  # second half
array = [14, -80, 20, 20, 20, 20, 2, 2, 2, 2, 2, 2, 2]  # igual
# array = [10, -80, 20, 20, 20, 20, 2, 2, 2, 2, 2]  # second half
# array = [10, -22, 33, -6, 42, 72, -36, -12, 52, 40, 80]


def can_be_partitioned(array):
    half = int(len(array) / 2)
    final_difference = 9999
    message = "can be partitioned, from 0 to {end} and {end} to {len_array}, difference: {diff}"
    if sum(array[:half]) > sum(array[half:]):
        suma = array[0]
        index = 0
        for x in range(len(array) - 1):
            suma = suma + array[x + 1]
            difference = suma - sum(array[x + 2:])
            if difference < 0:
                difference = difference * (-1)
            if final_difference > difference:
                final_difference = difference
                index = x
            if final_difference == 0:
                result = message.format(
                    end=index + 2,
                    len_array=len(array),
                    diff=final_difference
                )
                print("subarray a: ", array[:index + 2], ", sum:", sum(array[:index + 2]))
                print("subarray b: ", array[index + 2:], ", sum:", sum(array[index + 2:]))
                return result

        result = message.format(
            end=index + 2,
            len_array=len(array),
            diff=final_difference
        )
        print("subarray a: ", array[:index + 2], ", sum:", sum(array[:index + 2]))
        print("subarray b: ", array[index + 2:], ", sum:", sum(array[index + 2:]))
        return result
    elif sum(array[:half]) < sum(array[half:]):
        suma = array[-1]
        index = 0
        for x in range(len(array)-1, 1, -1):
            suma = suma + array[x - 1]
            difference = suma - sum(array[:x - 1])
            if difference < 0:
                difference = difference * (-1)

            if final_difference > difference:
                final_difference = difference
                index = x

            if final_difference == 0:
                result = message.format(
                    end=index - 1,
                    len_array=len(array),
                    diff=final_difference
                )
                print("subarray a: ", array[:index - 1], ", sum:", sum(array[:index - 1]))
                print("subarray b: ", array[index - 1:], ", sum:", sum(array[index - 1:]))
                return result

        result = message.format(
            end=index + 2,
            len_array=len(array),
            diff=final_difference
        )
        print("subarray a: ", array[:index - 1], "suma:", sum(array[:index - 1]))
        print("subarray b: ", array[index - 1:], "suma:", sum(array[index - 1:]))
        return result
    else:
        return \
            "can be partitioned, at the half of the array. 🐧 (half_length={half})". \
                format(
                half=half
            )


result = can_be_partitioned(array)
print("\nresult:", result)
