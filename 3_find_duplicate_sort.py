array = [17, 18, 1, 10, 14, 16, 2, 3, 11, 4, 5, 12, 6, 13, 7, 8, 11, 9, 19, 20]   # duplicate: 11
# array = [1, 2, 3, 4, 4, 5]


def find_duplicate_value(array):
    array.sort()

    for x in range(len(array)-1):
        if array[x] == array[x+1]:
            return array[x]

    return "actually, there is no duplicate number :)"


result_array = find_duplicate_value(array)
print("duplicate number:", result_array)
