array = [1, 2, 3, 1, 4, 4, 5, 3, 2, 5, 2]
# array = [1, 2, 2, 2]


def removes_duplicate_values(array):
    x = 0
    while x < len(array):
        for k in range(0, len(array)-1):
            if array[x] == array[k] and x != k:
                array.pop(k)
        x += 1
    return array


result_array = removes_duplicate_values(array)
print("array after duplicates have been removed:", result_array)
