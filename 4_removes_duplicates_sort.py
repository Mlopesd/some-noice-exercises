array = [1, 2, 3, 3, 3, 1, 4, 4, 5, 3, 2, 5, 2]
# array = [1, 2, 2, 2]


def removes_duplicate_values(array):
    array.sort()
    value = array[0]
    x = 0

    while x < len(array) - 1:
        if value != array[x]:
            value = array[x]

        if value == array[x + 1]:
            array.pop(x + 1)
        else:
            x += 1
    return array


result_array = removes_duplicate_values(array)
print("array after duplicates have been removed:", result_array)
