array = [17, 18, 1, 10, 14, 16, 2, 3, 11, 4, 5, 12, 6, 13, 7, 8, 14, 9, 19, 20]   # duplicate: 14
# array = [1, 2, 3, 4, 4, 5]


def find_duplicate_value(array):
    array_b = list(array)

    for x in range(0, len(array)):
        for k in range(1, len(array_b)):
            if array[x] == array_b[k]:
                return array[x]
        array_b.pop(0)
    return "actually, there is no duplicate number :)"


result_array = find_duplicate_value(array)
print("duplicate number:", result_array)
