array = [17, 18, 1, 10, 14, 16, 2, 3, 11, 4, 5, 12, 6, 13, 7, 8, 9, 19, 20]        # missing: 15
# array_a = [4, 2, 1]
# array_a = [3, 4, 6, 2, 1]


def missing_value(array):
    array.sort()

    for x in range(len(array)):
        if array[x] != x+1:
            return x+1

    return "actually, there is no missing number :)"


result_array = missing_value(array)
print("missing number:", result_array)
